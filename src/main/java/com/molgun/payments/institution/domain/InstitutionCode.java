package com.molgun.payments.institution.domain;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by molgun on 16/05/2017.
 */
public class InstitutionCode {

    public InstitutionCode() {
    }

    private String institutionCode;

    private InstitutionCode(String institutionCode) {
        this.institutionCode = institutionCode;
    }

    public static InstitutionCode create(String institutionCode) {
        Validate.notNull(institutionCode);
        return new InstitutionCode(institutionCode);
    }

    public String getInstitutionCode() {
        return institutionCode;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(institutionCode)
                .toHashCode();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final InstitutionCode institutionCode = (InstitutionCode) o;

        return sameValueAs(institutionCode);
    }

    public boolean sameValueAs(InstitutionCode subscription) {
        return subscription != null && new EqualsBuilder()
                .append(institutionCode, subscription.getInstitutionCode())
                .isEquals();
    }
}
