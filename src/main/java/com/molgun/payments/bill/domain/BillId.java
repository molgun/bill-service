package com.molgun.payments.bill.domain;

import org.apache.commons.lang3.Validate;

import java.io.Serializable;

/**
 * Created by molgun on 17/05/2017.
 */
public class BillId implements Serializable {

    private String id;

    public BillId(String id) {
        Validate.notNull(id);
        this.id = id;
    }

    public BillId() {
    }

    public String getId() {
        return id;
    }
}
