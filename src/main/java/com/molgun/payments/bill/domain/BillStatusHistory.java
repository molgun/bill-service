package com.molgun.payments.bill.domain;

import com.molgun.payments.creditcard.domain.CreditCard;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by molgun on 18/05/2017.
 */
public class BillStatusHistory {

    private BillStatus billStatus;
    private CreditCard creditCard;
    private ProcessTime processTime;
    private ProvisionId provisionId;

    public BillStatusHistory(BillStatus billStatus, CreditCard creditCard, ProcessTime processTime) {
        this.billStatus = billStatus;
        this.creditCard = creditCard;
        this.processTime = processTime;
    }

    public BillStatus getBillStatus() {
        return billStatus;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public ProcessTime getProcessTime() {
        return processTime;
    }

    public ProvisionId getProvisionId() {
        return provisionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        BillStatusHistory that = (BillStatusHistory) o;

        return new EqualsBuilder()
                .append(billStatus, that.billStatus)
                .append(creditCard, that.creditCard)
                .append(processTime, that.processTime)
                .append(provisionId, that.provisionId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(billStatus)
                .append(creditCard)
                .append(processTime)
                .append(provisionId)
                .toHashCode();
    }
}
