package com.molgun.payments.bill.application;

import com.molgun.payments.creditcard.domain.CreditCard;
import com.molgun.payments.bill.domain.Bill;

/**
 * Created by molgun on 16/05/2017.
 */
public interface PaymentEvents {

    public void paymentRequested();

    public void paid(Bill bill, CreditCard paymentSource);

    public void paidBillCanceled(Bill bill);
}
