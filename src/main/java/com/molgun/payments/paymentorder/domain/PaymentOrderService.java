package com.molgun.payments.paymentorder.domain;

import com.molgun.payments.creditcard.domain.CreditCard;
import com.molgun.payments.bill.domain.Customer;
import com.molgun.payments.bill.domain.Subscription;

/**
 * Created by molgun on 18/05/2017.
 */
public interface PaymentOrderService {

    public void giveOrder(Customer customer, Subscription subscription, CreditCard creditCard) throws PaymentOrderAlreadyExists;
}
