package com.molgun.payments.creditcard.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.math.BigDecimal;

/**
 * Created by molgun on 23/05/2017.
 */
public class PaymentDTO {

    private String cardNumber;
    private BigDecimal amount;
    private String trackingId;

    public PaymentDTO() {
    }

    public String toJson() {
        return "{\"cardNumber\": \"" + cardNumber + "\"," +
                "\"amount\": " + amount + "," +
                "\"trackingId\": \"" + trackingId + "\"}";
    }

    public PaymentDTO(String cardNumber, BigDecimal amount, String trackingId) {
        this.cardNumber = cardNumber;
        this.amount = amount;
        this.trackingId = trackingId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PaymentDTO that = (PaymentDTO) o;

        return new EqualsBuilder()
                .append(cardNumber, that.cardNumber)
                .append(amount, that.amount)
                .append(trackingId, that.trackingId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(cardNumber)
                .append(amount)
                .append(trackingId)
                .toHashCode();
    }
}
