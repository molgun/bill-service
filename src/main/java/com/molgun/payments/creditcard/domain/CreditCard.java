package com.molgun.payments.creditcard.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by molgun on 16/05/2017.
 */
public class CreditCard {

    private String cardNumber;

    public CreditCard() {
    }

    public CreditCard(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(cardNumber)
                .toHashCode();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final CreditCard creditCard = (CreditCard) o;

        return sameValueAs(creditCard);
    }

    public boolean sameValueAs(CreditCard creditCard) {
        return creditCard != null && new EqualsBuilder()
                .append(cardNumber, creditCard.getCardNumber())
                .isEquals();
    }
}
