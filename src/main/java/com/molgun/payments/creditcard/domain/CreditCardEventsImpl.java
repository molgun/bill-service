package com.molgun.payments.creditcard.domain;

import com.molgun.payments.bill.domain.BillService;

public class CreditCardEventsImpl implements CreditCardEvents {

    private BillService billService;

    public CreditCardEventsImpl(BillService billService) {
        this.billService = billService;
    }

    @Override
    public void couldNotProvisioned(TrackingId trackingId) {
    }
}
