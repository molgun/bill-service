package com.molgun.payments.creditcard.domain;

import com.molgun.payments.bill.domain.Amount;

/**
 * Created by molgun on 18/05/2017.
 */
public class CreditCardServiceImpl implements CreditCardService {

    private CreditCardEvents creditCardEvents;

    public CreditCardServiceImpl(CreditCardEvents creditCardEvents) {
        this.creditCardEvents = creditCardEvents;
    }

    public void takeProvision(CreditCard creditCard, Amount amount, TrackingId trackingId) {
        if (Math.random() % 2 == 0) {
            creditCardEvents.couldNotProvisioned(trackingId);
        }
    }
}
