package com.molgun.payments.creditcard.domain;

import com.molgun.payments.bill.domain.Amount;

/**
 * Created by molgun on 18/05/2017.
 */
public interface CreditCardService {

    public void takeProvision(CreditCard creditCard, Amount amount, TrackingId trackingId);
}
