package com.molgun.payments.paymentorder.application;

import com.molgun.payments.bill.domain.*;
import com.molgun.payments.bill.domain.utils.TestUtils;
import com.molgun.payments.bill.infrastructure.H2BillRepository;
import com.molgun.payments.creditcard.domain.CreditCard;
import com.molgun.payments.paymentorder.domain.PaymentOrder;
import com.molgun.payments.paymentorder.infrastructure.H2PaymentOrderRepository;
import com.molgun.payments.institution.domain.InstitutionCode;
import com.molgun.payments.paymentorder.domain.PaymentOrderAlreadyExists;
import com.molgun.payments.paymentorder.domain.PaymentOrderId;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PaymentOrderServiceIT {

    @Autowired
    H2BillRepository billRepository;

    @Autowired
    H2PaymentOrderRepository paymentOrderRepository;

    @Before
    public void init() throws InvalidCustomerId {
        billRepository.deleteAll();
        paymentOrderRepository.deleteAll();
        Subscription subscription = Subscription.create(InstitutionCode.create("TRKCLL"), SubscriptionId.create("313232"));
        Subscription subscription2 = Subscription.create(InstitutionCode.create("TRKCLL"), SubscriptionId.create("232324"));
        billRepository.save(new Bill(TestUtils.createBillId(), subscription, TestUtils.createAmount(), TestUtils.createDueDate()));
        billRepository.save(new Bill(TestUtils.createBillId("142142"), subscription2, TestUtils.createAmount(), TestUtils.createDueDate()));

        PaymentOrder order = PaymentOrder.create(PaymentOrderId.create("123122"), CustomerId.create("123123"),
                Subscription.create(InstitutionCode.create("TRKCLL"), SubscriptionId.create("313232")),
                TestUtils.createCreditCard());
        paymentOrderRepository.save(order);
    }

    @Test(expected = PaymentOrderAlreadyExists.class)
    public void given_customer_and_subscription_that_has_payment_order_when_payment_order_is_given_then_payment_order_is_not_created() throws SubscriptionAlreadyHasPaymentOrder, InvalidCustomerId, PaymentOrderAlreadyExists {
        Customer customer = TestUtils.createCustomer();
        Subscription subscription = Subscription.create(InstitutionCode.create("TRKCLL"), SubscriptionId.create("313232"));
        CreditCard creditCard = TestUtils.createCreditCard();
        PaymentOrderServiceImpl paymentOrderService = new PaymentOrderServiceImpl(paymentOrderRepository);
        paymentOrderService.giveOrder(customer, subscription, creditCard);
    }

    @Test
    public void given_subscription_when_payment_order_is_given_then_payment_order_is_created() throws InvalidCustomerId, PaymentOrderAlreadyExists {
        Customer customer = TestUtils.createCustomer();
        Subscription subscription = TestUtils.createSubscription();
        CreditCard creditCard = TestUtils.getCreditCards(customer).get(0);
        PaymentOrderServiceImpl paymentService = new PaymentOrderServiceImpl(paymentOrderRepository);
        paymentService.giveOrder(customer, subscription, creditCard);
        Assert.assertEquals(2, paymentOrderRepository.count());
    }
}
