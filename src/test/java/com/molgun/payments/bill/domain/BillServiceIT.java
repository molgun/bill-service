package com.molgun.payments.bill.domain;

import com.molgun.payments.bill.application.PaymentEvents;
import com.molgun.payments.bill.application.PaymentEventsImpl;
import com.molgun.payments.bill.infrastructure.H2BillRepository;
import com.molgun.payments.creditcard.domain.CreditCard;
import com.molgun.payments.creditcard.domain.CreditCardService;
import com.molgun.payments.creditcard.domain.ProvisionException;
import com.molgun.payments.creditcard.domain.TrackingId;
import com.molgun.payments.institution.domain.InstitutionCode;
import com.molgun.payments.paymentorder.domain.PaymentOrder;
import com.molgun.payments.paymentorder.domain.PaymentOrderId;
import com.molgun.payments.paymentorder.infrastructure.H2PaymentOrderRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.naming.OperationNotSupportedException;

import java.util.List;

import static com.molgun.payments.bill.domain.utils.TestUtils.*;
import static junit.framework.TestCase.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BillServiceIT {

    @Autowired
    H2BillRepository billRepository;

    @Autowired
    H2PaymentOrderRepository paymentOrderRepository;

    PaymentEvents paymentEvents;

    @Before
    public void init() throws InvalidCustomerId {
        paymentEvents = new PaymentEventsImpl(new CreditCardService() {
            @Override
            public void takeProvision(CreditCard creditCard, Amount amount, TrackingId trackingId) {
            }
        });
        Subscription subscription = Subscription.create(InstitutionCode.create("TRKCLL"), SubscriptionId.create("313232"));
        Subscription subscription2 = Subscription.create(InstitutionCode.create("TRKCLL"), SubscriptionId.create("232324"));
        billRepository.save(new Bill(createBillId(), subscription, createAmount(), createDueDate()));
        billRepository.save(new Bill(createBillId("142142"), subscription2, createAmount(), createDueDate()));

        PaymentOrder order = PaymentOrder.create(PaymentOrderId.create("123122"), CustomerId.create("123123"),
                Subscription.create(InstitutionCode.create("TRKCLL"), SubscriptionId.create("313232")),
                createCreditCard());
        paymentOrderRepository.save(order);
    }

    @Test
    public void given_subscription_when_bill_due_date_comes_then_bill_should_be_paid() throws InvalidCustomerId, OperationNotSupportedException, ProvisionException {
        BillService billService = new BillService(billRepository, paymentOrderRepository, paymentEvents);
        billService.payDueBills();
        long unpaid = billRepository.countByBillStatus(BillStatus.UNPAID);
        Assert.assertEquals(1, unpaid);
    }

    @Test
    public void given_costumer_and_unpaid_bill_when_costumer_selects_credit_card_and_pays_unpaid_bill_then_bill_should_be_paid() throws InvalidCustomerId, ProvisionException, OperationNotSupportedException {
        Customer customer = createCustomer();
        Bill bill = createBill();
        List<CreditCard> creditCards = getCreditCards(customer);
        BillService billService = new BillService(billRepository, paymentOrderRepository, paymentEvents);
        billService.pay(bill, creditCards.get(0));
        assertEquals(BillStatus.PAID, bill.getBillStatus());
    }

    @Test
    public void given_costumer_and_unpaid_bill_when_costumer_selects_credit_card_that_has_no_limit_and_pays_unpaid_bill_then_bill_should_be_unpaid() throws InvalidCustomerId, OperationNotSupportedException {
//        Customer customer = createCustomer();
//        Bill bill = createBill();
//        List<CreditCard> creditCards = getCreditCards(customer);
//        PaymentEvents pe = new PaymentEventsImpl(new CreditCardService() {
//            @Override
//            public void takeProvision(CreditCard creditCard, Amount amount, TrackingId trackingId) {
//
//            }
//        });
//        BillService billService = new BillService(billRepository, paymentOrderRepository, pe);
//        billService.pay(bill, creditCards.get(0));
//        bill = billRepository.findOne(bill.getBillId());
//        assertEquals(BillStatus.UNPAID, bill.getBillStatus());
    }
}
