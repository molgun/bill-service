package com.molgun.payments.bill.domain.utils;

import com.molgun.payments.bill.application.PaymentEventsImpl;
import com.molgun.payments.bill.domain.*;
import com.molgun.payments.bill.infrastructure.InMemoryBillRepository;
import com.molgun.payments.creditcard.domain.CreditCard;
import com.molgun.payments.creditcard.domain.CreditCardService;
import com.molgun.payments.creditcard.domain.TrackingId;
import com.molgun.payments.institution.domain.InstitutionCode;
import com.molgun.payments.paymentorder.application.PaymentOrderServiceImpl;
import com.molgun.payments.paymentorder.domain.PaymentOrder;
import com.molgun.payments.paymentorder.domain.PaymentOrderId;
import com.molgun.payments.paymentorder.infrastructure.InMemoryPaymentOrderRepository;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class TestUtils {
    public static InMemoryBillRepository createBillRepository() {
        InMemoryBillRepository repository = new InMemoryBillRepository();
        Subscription subscription = Subscription.create(InstitutionCode.create("TRKCLL"), SubscriptionId.create("313232"));
        Subscription subscription2 = Subscription.create(InstitutionCode.create("TRKCLL"), SubscriptionId.create("232324"));
        repository.save(new Bill(createBillId(), subscription, createAmount(), createDueDate()));
        repository.save(new Bill(createBillId("142142"), subscription2, createAmount(), createDueDate()));
        return repository;
    }

    public static BillService createBillService(InMemoryBillRepository inMemoryBillRepository) throws InvalidCustomerId {
        return createBillService(inMemoryBillRepository, createPaymentOrderRepository());
    }

    public static BillService createBillService(InMemoryBillRepository inMemoryBillRepository, InMemoryPaymentOrderRepository paymentOrderRepository) throws InvalidCustomerId {
        return new BillService(inMemoryBillRepository, paymentOrderRepository, new PaymentEventsImpl(new CreditCardService() {
            @Override
            public void takeProvision(CreditCard creditCard, Amount amount, TrackingId trackingId) {
                System.out.println("Provision! " + amount.getAmount());
            }
        }));
    }

    public static InMemoryPaymentOrderRepository createPaymentOrderRepository() throws InvalidCustomerId {
        InMemoryPaymentOrderRepository repository = new InMemoryPaymentOrderRepository();
        PaymentOrder order = PaymentOrder.create(PaymentOrderId.create("123123"), CustomerId.create("123123"),
                Subscription.create(InstitutionCode.create("TRKCLL"), SubscriptionId.create("313232")),
                createCreditCard());
        repository.save(order);
        return repository;
    }

    public static PaymentOrderServiceImpl createPaymentOrderService(InMemoryPaymentOrderRepository repository) {
        return new PaymentOrderServiceImpl(repository);
    }

    public static Subscription createSubscription() {
        return new Subscription(InstitutionCode.create("TRKCLL"), createSubscriptionId());
    }

    public static Cash createCash() {
        return new Cash();
    }

    public static Bill createBill() {
        return new Bill(createBillId(), createSubscription(), createAmount(), createDueDate());
    }

    public static DueDate createDueDate() {
        return new DueDate(new Date());
    }

    public static Amount createAmount() {
        return new Amount(BigDecimal.ONE);
    }

    public static BillId createBillId() {
        return createBillId("135135");
    }

    public static BillId createBillId(String id) {
        return new BillId(id);
    }

    public static PotentialCostumer createPotentialCostumer() {
        return new PotentialCostumer();
    }

    public static SubscriptionId createSubscriptionId() {
        return new SubscriptionId("123333");
    }

    public static List<CreditCard> getCreditCards(Customer customer) {
        return Arrays.asList(new CreditCard("123123123"));
    }

    public static Customer createCustomer(CustomerId customerId) {
        return Customer.create(customerId);
    }

    public static Customer createCustomer() throws InvalidCustomerId {
        return Customer.create(CustomerId.create("123123"));
    }

    public static CreditCard createCreditCard() {
        return new CreditCard("123123123123");
    }
}
