FROM openjdk:8
ENTRYPOINT ["/usr/bin/java", "-jar", "/usr/share/bill-service/bill-service.jar"]

ADD target/bill-service.jar /usr/share/bill-service/bill-service.jar